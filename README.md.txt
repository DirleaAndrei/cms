The project is a web page for employee management.
The page contain a list will all employees, a modal with a form for registering a new employee, and for every employee 2 buttons, one for
edit the employee`s data and another one for delete the employee.
The technologies used for this project are html, css, javascript and bootstrap.