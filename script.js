var addForm = document.querySelector("#form1");
var editForm = document.querySelector("#form2");
var employees = [];
var employee = {};
var inputs = document.querySelectorAll("input");
var gender = document.querySelectorAll("select");
var updatedImage;
var uploadImage;

//get data from db and store it to employees array
async function getDbData() {
    await db.collection("Employees").get().then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
            let employee = doc.data();
            employee.id = doc.id;
            employees.push(employee);
        });
    });
    showData(employees);
}

getDbData();

// get image data
img = document.getElementById("img");
img.addEventListener("change", function () {
    uploadImage = this.files[0];
})
imgEdit = document.getElementById("imgEdit");
imgEdit.addEventListener("change", function () {
    updatedImage = this.files[0];
})

 // delete from DB    
 async function deleteCard() {
    employeeToDelete = employees.filter(employee => employee.id === this.parentNode.parentNode.id)[0]; 
    await db.collection("Employees").doc(employeeToDelete.id).delete()
    location.reload();
 }   

//Populate from database
async function showData(employees) {
    document.getElementById('employee-list').innerHTML = '';
    employees.forEach(employee => {
        addEmployee(employee);
    });
    //create delete function for all cards from array
    var buttons = document.querySelectorAll(".btn-danger");
    buttons.forEach(button => {
        button.addEventListener('click', deleteCard);
    })
    // create edit function for all cards from array
    var editButtons = document.querySelectorAll(".btn-warning");
    editButtons.forEach(button => {
        button.addEventListener('click', editCard);
    })
}


// Submit form
addForm.addEventListener("submit", async function (e) {
    e.preventDefault();

    //Check if image was uploaded
    upload = true;
    if (typeof (uploadImage) === 'undefined') {
        uploadImage = {};
        uploadImage.name = 'new_user.jpg';
        upload = false;
    }

    //Create ref
    let photo = 'images/' + uploadImage.name;
    var storageRef = await strg.ref(photo);

    //upload image
    if (upload === true) {
        await storageRef.put(uploadImage);
    }

    //upload data
    await db.collection("Employees").add({
        firstName: addForm.firstName.value,
        secondName: addForm.secondName.value,
        email: addForm.email.value,
        birthday: addForm.date1.value,
        gender: addForm.gender.value,
        photo: photo
    });
    location.reload();
});

// bind employee to edit
function editCard() {
    employee = employees.filter(employee => employee.id === this.parentNode.parentNode.id)[0]; 
    editSetInputFields(employee);
}


// Edit form
editForm.addEventListener("submit", async function (e) {
    e.preventDefault();
    photo = employee.photo;
    //check if another image was uploaded
    if (typeof (updatedImage) !== 'undefined') {
        //Create ref
        photo = 'images/' + updatedImage.name;
        var storageRef = await strg.ref(photo);
        //get error code for check if image already exist
        // storageRef.child(photo).getDownloadURL().then(onResolve, onReject);
        // function onResolve(foundURL) {
        //     console.log();
        // }
        // function onReject(error) {
        //     if(error.code === "storage/object-not-found")
        //     update = true;
        // }

        //upload image
        await storageRef.put(updatedImage);
    }

    await db.collection("Employees").doc(employee.id).set({
        firstName: editForm.firstNameEdit.value,
        secondName: editForm.secondNameEdit.value,
        email: editForm.emailEdit.value,
        birthday: editForm.date1Edit.value,
        gender: editForm.genderEdit.value,
        photo: photo
    });
    location.reload();
});


function editSetInputFields(employee1) {
    inputs.forEach(input => {
        if (input.id === "firstNameEdit") {
            input.value = employee1.firstName;
        }
        if (input.id === "secondNameEdit") {
            input.value = employee1.secondName;
        }
        if (input.id === "emailEdit") {
            input.value = employee1.email;
        }
        if (input.id === "date1Edit") {
            input.value = employee1.birthday;
        }
    });
    if (typeof (employee1.photo) === "undefined" || typeof(employee1.photo) === 'string') {
    
    } else {
        document.getElementById("imgEdit").src = "";
        document.getElementById("imgEdit").value = "";
    }
    gender.forEach(select => {
        if (select.id === "genderEdit") {
            select.value = employee1.gender;
        }
    });
    employee = employee1;
}

// Used for adding
function formatDate(employee) {
    let mounth = {
        '01': "January",
        '02': "February",
        '03': "March",
        '04': "April",
        '05': "May",
        '06': "June",
        '07': "July",
        '08': "August",
        '09': "September",
        '10': "October",
        '11': "November",
        '12': "December"
    }
    let mounthInNum = employee.birthday.substring(5, 7);
    let mounthInString = mounth[mounthInNum];
    formatedDate = employee.birthday.substring(8, 10) + ' ' + mounthInString + ' ' + employee.birthday.substring(0, 4);
    return formatedDate;
}

//Clear submit form
document.getElementById("clear1").addEventListener('click', () => {
    addForm.reset();
});

//Clear edit form
document.getElementById("clear2").addEventListener('click', () => {
    editForm.reset();
});


// load the image from db and show them
async function getImages(employee) {
    let ref = await strg.ref();
    imgURL = await ref.child(employee.photo).getDownloadURL();
    
}

//Create Employee object on page
function addEmployee(employee) {   
    let body = document.getElementById('body');
    let container = document.getElementById('div1');
    let list = document.getElementById('employee-list');
    let employeeContainer = document.createElement('div');
    employeeContainer.classList.add("card");
    employeeContainer.classList.add("div-employee");
    employeeContainer.id = employee.id;
    let imgName = document.createElement('div');
    imgName.classList.add("employee-img-name");
    let img = document.createElement('img');
    img.classList.add("employee-img");

    let prom = new Promise((resolve) => {
        let ref = strg.ref();
        imgURL = ref.child(employee.photo).getDownloadURL();
        resolve(imgURL);
    });
    prom.then(result => {
        img.src = result;
    });


    img.alt = "Employee image";
    let name = document.createElement('div');
    name.classList.add("employee-name");
    let firstName = document.createElement('p');
    firstName.appendChild(document.createTextNode(employee.firstName));
    let secondName = document.createElement('p');
    secondName.appendChild(document.createTextNode(employee.secondName));
    let emailBirthday = document.createElement('div');
    emailBirthday.classList.add("card-body");
    emailBirthday.classList.add("employee-email-birthday");
    let email = document.createElement('h5');
    email.classList.add("card-title");
    email.appendChild(document.createTextNode(employee.email));
    let birthday = document.createElement('h5');
    birthday.classList.add("card-title");
    birthday.appendChild(document.createTextNode(formatDate(employee)));
    let gender = document.createElement('h5');
    gender.classList.add("card-title");
    gender.appendChild(document.createTextNode(employee.gender));
    let employeeButtons = document.createElement('div');
    employeeButtons.classList.add("employee-buttons");
    let employeeEdit = document.createElement('button');
    employeeEdit.classList.add("btn");
    employeeEdit.classList.add("btn-warning");
    employeeEdit.appendChild(document.createTextNode("Edit"));
    employeeEdit.setAttribute("data-toggle", "modal");
    employeeEdit.setAttribute("data-target", "#editEmployee");
    let employeeDelete = document.createElement('button');
    employeeDelete.classList.add("btn");
    employeeDelete.classList.add("btn-danger");
    employeeDelete.appendChild(document.createTextNode("Delete"));

    // create card for employee
    body.appendChild(container);
    container.appendChild(list);
    list.appendChild(employeeContainer);
    employeeContainer.appendChild(imgName);
    imgName.appendChild(img);
    imgName.appendChild(name);
    name.appendChild(firstName);
    name.appendChild(secondName);
    employeeContainer.appendChild(emailBirthday);
    emailBirthday.appendChild(email);
    emailBirthday.appendChild(birthday);
    emailBirthday.appendChild(gender);
    employeeContainer.appendChild(employeeButtons);
    employeeButtons.appendChild(employeeEdit);
    employeeButtons.appendChild(employeeDelete);
};
    
//filter by image
filterByImage = document.querySelector("#hasProfile");
filterByImage.addEventListener('change',() => {
    console.log(filterByImage.value);
})
